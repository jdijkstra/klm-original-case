# Travel API Client

## Getting started

Clone this repo and start it (on windows systems use the gradlew.bat file):

```bash
# start the backend
./gradlew bootRun -Pargs=--spring.security.oauth2.client.registration.klm.client-secret=<travel API client secret>

# start the frontend
cd src/main/frontend
nvm use
npm start
```

Make sure that the [Travel API Mock](https://bitbucket.org/afklmdevnet/simple-travel-api-mock) is running as well!

## Build

To build a production bundle:

```bash
./gradlew -Penv=prod build
```

## Implementation decisions

The following functionality was intentionally dropped to stay within the scope and time limit of the assignment:
* JavaDoc/tsdoc
    * Class and method names are assumed to be self-explaining in this small domain
* CORS
    * Actual configuration is better handled by the infrastructure
* Minimum response time in the metrics
    * Not provided by the default metrics
    * This would be a point of discussion with the PO to balance the need vs effort
* Returning the request ID in the response
    * For production something like Sleuth would be a better option
* Running the frontend tests in the Gradle build
* Edge/corner cases in the tests
* E2E tests
