package com.afkl.cases.df.api.fare;

import java.math.BigDecimal;

import com.afkl.cases.df.SecurityConfiguration;
import com.afkl.cases.df.travelclient.TravelApiClient;
import com.afkl.cases.df.travelclient.domain.Fare;
import com.afkl.cases.df.travelclient.domain.Location;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = FareController.class)
@Import(SecurityConfiguration.class)
class FareControllerTest {

    @MockBean
    private TravelApiClient travelApiClient;

    @Autowired
    private WebTestClient webClient;

    @Test
    void fetchFareShouldFetchFareAndLocationDetails() {
        when(travelApiClient.fetchFare("A", "B")).thenReturn(Mono.just(new Fare(
            BigDecimal.TEN, "EUR",
            "A", "B"
        )));
        when(travelApiClient.fetchLocationByCode("A")).thenReturn(Mono.just(new Location(
            "A", "Stairs", "Apples and pears rhymes with stairs"
        )));
        when(travelApiClient.fetchLocationByCode("B")).thenReturn(Mono.just(new Location(
            "B", "Trouble", "Barney Rubble rhymes with trouble"
        )));

        webClient.get()
            .uri(uriBuilder ->
                uriBuilder
                .path("/api/fares/{origin}/{destination}")
                .build("A", "B"))
            .exchange()
            .expectStatus().is2xxSuccessful()
            .expectBody()
                .jsonPath("origin.name").isEqualTo("Stairs")
                .jsonPath("destination.name").isEqualTo("Trouble")
                .jsonPath("amount").isEqualTo(BigDecimal.TEN)
                .jsonPath("currency").isEqualTo("EUR");
    }
}
