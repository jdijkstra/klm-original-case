package com.afkl.cases.df.api.location;

import com.afkl.cases.df.SecurityConfiguration;
import com.afkl.cases.df.travelclient.TravelApiClient;
import com.afkl.cases.df.travelclient.domain.Location;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;

import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = LocationController.class)
@Import(SecurityConfiguration.class)
class LocationControllerTest {

    @MockBean
    private TravelApiClient travelApiClient;

    @Autowired
    private WebTestClient webClient;

    @Test
    void findLocationsShouldReturnResultFromApi() {
        when(travelApiClient.findLocations("foobar")).thenReturn(Flux.just(
            new Location("NL", "NLD", "Netherlands"),
            new Location("NC", "NCL", "New Caledonia")
        ));
        webClient.get()
            .uri(uriBuilder -> uriBuilder
                .path("/api/locations")
                .queryParam("term", "foobar")
                .build())
            .exchange()
            .expectStatus().is2xxSuccessful()
            .expectBody()
            .jsonPath("[0].name").isEqualTo("NLD")
            .jsonPath("[1].name").isEqualTo("NCL")
            .jsonPath("[2]").doesNotExist();

    }
}
