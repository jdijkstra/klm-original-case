import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { EMPTY, Observable, of } from 'rxjs';
import { Location } from './domain/location';
import { Fare } from './domain/fare';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TravelService {

  constructor(private http: HttpClient) { }

  search(term: string): Observable<Location[]> {
    if (term != null && term !== '') {
      const params = new HttpParams().set('term', term);
      return this.http.get<Location[]>('http://localhost:9000/api/locations', { params });
    } else {
      return of([]);
    }
  }

  fetchFare(origin: Location, destination: Location): Observable<Fare> {
    if (origin != null && destination != null) {
      return this.http.get<Fare>(`http://localhost:9000/api/fares/${origin.code}/${destination.code}`);
    } else {
      return EMPTY;
    }
  }
}
