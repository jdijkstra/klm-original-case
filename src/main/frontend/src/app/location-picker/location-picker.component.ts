import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { TravelService } from '../travel.service';
import { Location } from '../domain/location';
import { NgbTypeaheadSelectItemEvent } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-location-picker',
  templateUrl: './location-picker.component.html',
  styleUrls: ['./location-picker.component.less']
})
export class LocationPickerComponent {

  @Input()
  label: string;

  @Output()
  picked = new EventEmitter<Location>();

  constructor(private locationService: TravelService) {
  }

  model: Location;
  searching = false;
  searchFailed = false;

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      tap(() => this.searching = true),
      switchMap(searchTerm =>
        this.locationService.search(searchTerm).pipe(
          tap(() => this.searchFailed = false),
          catchError(() => {
            this.searchFailed = true;
            return of([]);
          })
        )
      ),
      tap(
        () =>
          this
            .searching = false
      )
    );

  formatter = (location: Location) => location.description;

  onItemSelected(event: NgbTypeaheadSelectItemEvent) {
    this.picked.emit(event.item);
  }
}
