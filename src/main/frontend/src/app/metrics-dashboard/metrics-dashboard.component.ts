import { Component, OnDestroy, OnInit } from '@angular/core';
import { EMPTY, Subscription } from 'rxjs';
import { Metrics, MetricsService } from '../metrics.service';
import { catchError, onErrorResumeNext, tap } from 'rxjs/operators';

@Component({
  selector: 'app-metrics-dashboard',
  templateUrl: './metrics-dashboard.component.html',
  styleUrls: ['./metrics-dashboard.component.less']
})
export class MetricsDashboardComponent implements OnInit, OnDestroy {

  subscription: Subscription;
  metrics: Metrics;
  requestFailed = false;

  constructor(private metricsService: MetricsService) { }

  fetchMetrics() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    this.requestFailed = false;
    this.subscription = this.metricsService.fetch()
        .pipe(
            tap(() => this.requestFailed = false),
            catchError(() => {
              this.requestFailed = true;
              return EMPTY;
            })
        )

        .subscribe(metrics => this.metrics = metrics);
  }

  ngOnInit() {
    this.fetchMetrics();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
