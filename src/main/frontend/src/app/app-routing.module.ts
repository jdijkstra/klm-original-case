import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TripSelectorComponent } from './trip-selector/trip-selector.component';
import { MetricsDashboardComponent } from './metrics-dashboard/metrics-dashboard.component';

const routes: Routes = [
  { path: 'search', component: TripSelectorComponent },
  { path: 'metrics', component: MetricsDashboardComponent },
  { path: '',   redirectTo: '/search', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
