import { TravelService } from './travel.service';
import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('TravelService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let travelService: TravelService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    travelService = TestBed.get(TravelService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  describe('search', () => {
    it('should pass the search term to the backend', () => {
      travelService.search('some place I\'m looking for').subscribe(
        results => expect(results).toEqual([]),
        fail
      );

      const requestInfo = httpTestingController.expectOne(request => request.url === 'http://localhost:9000/api/locations');
      expect(requestInfo.request.method).toEqual('GET');
      expect(requestInfo.request.params.get('term')).toEqual('some place I\'m looking for');
      requestInfo.flush([]);
    });
  });

  describe('fetchFare', () => {
    it('should pass the location codes to the backend', () => {
      travelService.fetchFare({code: 'A'} as any, {code: 'B'} as any).subscribe(
        results => expect(results).toEqual({} as any),
        fail
      );

      const requestInfo = httpTestingController.expectOne('http://localhost:9000/api/fares/A/B');
      expect(requestInfo.request.method).toEqual('GET');
      requestInfo.flush({});
    });
  });
});
