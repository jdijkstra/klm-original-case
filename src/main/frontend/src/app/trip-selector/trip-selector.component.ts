import { Component, OnDestroy, OnInit } from '@angular/core';
import { combineLatest, forkJoin, Observable, of, Subject, Subscription } from 'rxjs';
import { Location } from '../domain/location';
import { catchError, combineAll, distinctUntilChanged, filter, mergeMap, switchMap, tap } from 'rxjs/operators';
import { TravelService } from '../travel.service';
import { Fare } from '../domain/fare';

@Component({
  selector: 'app-trip-selector',
  templateUrl: './trip-selector.component.html',
  styleUrls: ['./trip-selector.component.less']
})
export class TripSelectorComponent implements OnInit, OnDestroy{

  origin$ = new Subject<Location>();
  destination$ = new Subject<Location>();
  subscription: Subscription;
  fare: Fare;
  searching = false;
  searchFailed = false;

  constructor(private travelService: TravelService) {
  }

  ngOnInit(): void {
    this.subscription = combineLatest(this.origin$, this.destination$).pipe(
      distinctUntilChanged(),
      filter(([origin, destination]) => origin && destination && origin.code !== destination.code),
      tap(() => this.searching = true),
      mergeMap(([origin, destination]) => this.travelService.fetchFare(origin, destination).pipe(
        tap(() => this.searchFailed = false),
        catchError(() => {
          this.searchFailed = true;
          return of(null);
        })
      )),
      tap(() => this.searching = false)
    ).subscribe(result => this.fare = result);
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
