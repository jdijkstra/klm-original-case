import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TripSelectorComponent } from './trip-selector.component';
import { FormsModule } from '@angular/forms';
import { NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LocationPickerComponent } from '../location-picker/location-picker.component';

describe('TripSelectorComponent', () => {
  let component: TripSelectorComponent;
  let fixture: ComponentFixture<TripSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        NgbTypeaheadModule,
        HttpClientTestingModule
      ],
      declarations: [
        TripSelectorComponent,
        LocationPickerComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TripSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
