import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TripSelectorComponent } from './trip-selector/trip-selector.component';
import { LocationPickerComponent } from './location-picker/location-picker.component';
import { FormsModule } from '@angular/forms';
import { NgbCollapseModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { MetricsDashboardComponent } from './metrics-dashboard/metrics-dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    TripSelectorComponent,
    LocationPickerComponent,
    MetricsDashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgbTypeaheadModule,
    NgbCollapseModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
