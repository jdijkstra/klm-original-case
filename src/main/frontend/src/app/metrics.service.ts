import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, of, timer, zip } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';

const KEY_COUNT = 'COUNT';
const KEY_TOTAL_TIME = 'TOTAL_TIME';
const KEY_MAX_TIME = 'MAX';
const OUTCOME_SUCCESS = 'SUCCESS';
const OUTCOME_CLIENT_ERROR = 'CLIENT_ERROR';
const OUTCOME_SERVER_ERROR = 'SERVER_ERROR';


@Injectable({
  providedIn: 'root'
})
export class MetricsService {

  constructor(private http: HttpClient) {
  }

  fetch(): Observable<Metrics> {
    return timer(0, 5000).pipe(
      mergeMap(() => this.fetchRequestMetrics())
    );
  }

  private fetchRequestMetrics(): Observable<Metrics> {
    return this.http.get<MetricsResponse>('http://localhost:9000/actuator/metrics/http.server.requests').pipe(
      mergeMap(response => {
          const outcomes = response.availableTags.filter(tag => tag.tag === 'outcome')[0].values;
          const outcomeRequests = outcomes.map(outcome => this.fetchOutcomeCount(outcome).pipe(map(count => ({[outcome]: count}))));
          return zip(...outcomeRequests).pipe(
            map(outcomeResults => {
              const mainMeasurements = response.measurements.reduce(
                (accumulator, measurement) => Object.assign(accumulator, {[measurement.statistic]: measurement.value}),
                {} as { string: number }
              );
              const outcomeCounts = outcomeResults.reduce((acc, outcome) => Object.assign(acc, outcome), {} as { string: number });
              return {
                totalNumberOfRequests: mainMeasurements[KEY_COUNT],
                numberOfSuccessRequests: outcomeCounts[OUTCOME_SUCCESS] || 0,
                numberOfClientErrorRequests: outcomeCounts[OUTCOME_CLIENT_ERROR] || 0,
                numberOfServerErrorRequests: outcomeCounts[OUTCOME_SERVER_ERROR] || 0,
                totalResponseTime: mainMeasurements[KEY_TOTAL_TIME],
                averageResponseTime: mainMeasurements[KEY_TOTAL_TIME] / mainMeasurements[KEY_COUNT],
                maxResponseTime: mainMeasurements[KEY_MAX_TIME]
              } as Metrics;
            })
          );
        }
      )
    );
  }

  private fetchOutcomeCount(outcome: string): Observable<number> {
    const params = new HttpParams()
      .set('tag', `outcome:${outcome}`);
    return this.http.get<MetricsResponse>('http://localhost:9000/actuator/metrics/http.server.requests', {params}).pipe(
      map(response => +response.measurements[0].value),
      catchError(() => of(0))
    );
  }
}

interface MetricsResponse {
  name: string;
  measurements: Measurement[];
  availableTags: Tag[];
}

interface Measurement {
  statistic: string;
  value: number;
}

interface Tag {
  tag: string;
  values: string[];
}

export interface Metrics {
  totalNumberOfRequests: number;
  numberOfSuccessRequests: number;
  numberOfClientErrorRequests: number;
  numberOfServerErrorRequests: number;
  totalResponseTime: number;
  averageResponseTime: number;
  maxResponseTime: number;
}
