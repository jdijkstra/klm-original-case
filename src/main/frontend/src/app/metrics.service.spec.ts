import { fakeAsync, TestBed, tick } from '@angular/core/testing';

import { MetricsService } from './metrics.service';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('MetricsService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let metricsService: MetricsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    metricsService = TestBed.get(MetricsService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should request metrics for all outcomes', fakeAsync(() => {
    const rootResult = {
      name: 'testing',
      measurements: [
        {statistic: 'COUNT', value: 42},
        {statistic: 'TOTAL_TIME', value: 31337},
        {statistic: 'MAX', value: 1337}
      ],
      availableTags: [
        {tag: 'outcome', values: ['SERVER_ERROR', 'SUCCESS']}
      ]
    };
    const serverErrorOutcomeResult = {
      name: 'server_error',
      measurements: [
        {statistic: 'COUNT', value: 13}
      ],
      tags: []
    };
    const successOutcomeResult = {
      name: 'server_error',
      measurements: [
        {statistic: 'COUNT', value: 17}
      ],
      tags: []
    };

    const subscription = metricsService.fetch().subscribe(
      metrics => {
        expect(metrics).toEqual({
          totalNumberOfRequests: 42,
          numberOfSuccessRequests: 17,
          numberOfClientErrorRequests: 0,
          numberOfServerErrorRequests: 13,
          totalResponseTime: 31337,
          averageResponseTime: 31337 / 42,
          maxResponseTime: 1337
        });
        subscription.unsubscribe();
      },
      fail
    );

    tick(1);

    const rootRequestInfo = httpTestingController.expectOne('http://localhost:9000/actuator/metrics/http.server.requests');
    expect(rootRequestInfo.request.method).toEqual('GET');
    rootRequestInfo.flush(rootResult);

    const serverErrorOutcomeRequestInfo = httpTestingController.expectOne(request =>
      request.url === 'http://localhost:9000/actuator/metrics/http.server.requests'
      && request.params.get('tag') === 'outcome:SERVER_ERROR');
    expect(serverErrorOutcomeRequestInfo.request.method).toEqual('GET');
    serverErrorOutcomeRequestInfo.flush(serverErrorOutcomeResult);

    const successOutcomeRequestInfo = httpTestingController.expectOne(request =>
      request.url === 'http://localhost:9000/actuator/metrics/http.server.requests'
      && request.params.get('tag') === 'outcome:SUCCESS');
    expect(successOutcomeRequestInfo.request.method).toEqual('GET');
    successOutcomeRequestInfo.flush(successOutcomeResult);
  }));

});
