import { Location } from './location';

export interface Fare {
  origin: Location;
  destination: Location;
  amount: number;
  currency: string;
}
