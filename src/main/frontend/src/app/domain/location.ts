export interface Location {
  name: string;
  code: string;
  description: string;
}
