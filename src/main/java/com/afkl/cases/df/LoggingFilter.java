package com.afkl.cases.df;

import java.util.UUID;

import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

@Component
public class LoggingFilter implements WebFilter {

    private static final String REQUEST_ID_KEY = "requestid";

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
        MDC.put(REQUEST_ID_KEY, UUID.randomUUID().toString());
        return chain.filter(exchange).doFinally(ignored -> MDC.remove(REQUEST_ID_KEY));
    }
}
