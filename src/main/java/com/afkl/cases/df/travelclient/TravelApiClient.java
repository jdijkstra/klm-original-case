package com.afkl.cases.df.travelclient;

import java.util.List;

import com.afkl.cases.df.travelclient.domain.Fare;
import com.afkl.cases.df.travelclient.domain.Location;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class TravelApiClient {

    private final WebClient webClient;

    public TravelApiClient(WebClient webClient) {
        this.webClient = webClient;
    }

    public Flux<Location> findLocations(String searchTerm) {
        return webClient.get()
                .uri(uriBuilder ->
                        uriBuilder.path("/airports")
                                .queryParam("term", searchTerm)
                                .build()
                )
                .retrieve()
                .bodyToMono(LocationsResponse.class)
                .flatMapMany(response -> Flux.fromIterable(response.getEmbeddedLocations().getLocations()));
    }

    public Mono<Location> fetchLocationByCode(String locationCode) {
        return webClient.get()
                .uri(uriBuilder ->
                        uriBuilder.path("/airports/{code}")
                                .build(locationCode)
                )
                .retrieve()
                .bodyToMono(Location.class);
    }

    public Mono<Fare> fetchFare(String originCode, String destinationCode) {
        return webClient.get()
                .uri(uriBuilder ->
                        uriBuilder.path("/fares/{origin}/{destination}")
                                .build(originCode, destinationCode)
                )
                .retrieve()
                .bodyToMono(Fare.class);
    }
}

/**
 * Needed to unwrap the response for multiple locations
 */
class LocationsResponse {
    @JsonProperty("_embedded")
    private EmbeddedLocations embeddedLocations;

    public LocationsResponse() {
        // default constructor for jackson
    }

    public EmbeddedLocations getEmbeddedLocations() {
        return embeddedLocations;
    }

    public void setEmbeddedLocations(EmbeddedLocations embeddedLocations) {
        this.embeddedLocations = embeddedLocations;
    }
}

/**
 * Needed to unwrap the response for multiple locations
 */
class EmbeddedLocations {
    private List<Location> locations;

    public EmbeddedLocations() {
        // default constructor for jackson
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }
}

