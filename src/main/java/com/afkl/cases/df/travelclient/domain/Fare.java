package com.afkl.cases.df.travelclient.domain;

import java.math.BigDecimal;

public class Fare {
    private BigDecimal amount;
    private String currency;
    private String origin;
    private String destination;

    public Fare() {
        // default constructor for jackson
    }

    public Fare(BigDecimal amount, String currency, String origin, String destination) {
        this.amount = amount;
        this.currency = currency;
        this.origin = origin;
        this.destination = destination;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }
}
