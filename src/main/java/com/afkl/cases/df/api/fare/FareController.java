package com.afkl.cases.df.api.fare;

import com.afkl.cases.df.api.location.LocationDto;
import com.afkl.cases.df.travelclient.TravelApiClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@CrossOrigin
@RestController
@RequestMapping("/api/fares")
public class FareController {

    private final Logger logger = LoggerFactory.getLogger(FareController.class);

    private final TravelApiClient travelApiClient;

    public FareController(TravelApiClient travelApiClient) {
        this.travelApiClient = travelApiClient;
    }

    @GetMapping("/{origin}/{destination}")
    public Mono<FareDto> fetchFare(@PathVariable("origin") String originCode, @PathVariable("destination") String destinationCode) {
        logger.info("Fetching price and location details for trip from {} to {}", originCode, destinationCode);
        return Mono.zip(
                travelApiClient.fetchLocationByCode(originCode),
                travelApiClient.fetchLocationByCode(destinationCode),
                travelApiClient.fetchFare(originCode, destinationCode)
        ).map(results -> new FareDto(
                        new LocationDto(results.getT1()),
                        new LocationDto(results.getT2()),
                        results.getT3().getAmount(),
                        results.getT3().getCurrency()
                )
        ).doOnError(Throwable::printStackTrace);
    }


}
