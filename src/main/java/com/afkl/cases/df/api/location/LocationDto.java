package com.afkl.cases.df.api.location;

import com.afkl.cases.df.travelclient.domain.Location;

public class LocationDto {
    private final String code;
    private final String name;
    private final String description;

    public LocationDto(Location location) {
        this(location.getCode(), location.getName(), location.getDescription());
    }

    public LocationDto(String code, String name, String description) {
        this.code = code;
        this.name = name;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
