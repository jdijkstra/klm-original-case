package com.afkl.cases.df.api.location;

import com.afkl.cases.df.travelclient.TravelApiClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@CrossOrigin
@RestController
@RequestMapping("/api/locations")
public class LocationController {

    private static final Logger logger = LoggerFactory.getLogger(LocationController.class);

    private final TravelApiClient locationService;

    public LocationController(TravelApiClient locationService) {
        this.locationService = locationService;
    }

    @GetMapping()
    public Flux<LocationDto> findLocations(@RequestParam("term") String searchTerm) {
        logger.info("Searching for locations matching \"{}\"", searchTerm);
        return locationService.findLocations(searchTerm)
                .map(LocationDto::new);
    }
}
