package com.afkl.cases.df.api.fare;

import java.math.BigDecimal;

import com.afkl.cases.df.api.location.LocationDto;

public class FareDto {
    private final LocationDto origin;
    private final LocationDto destination;
    private final BigDecimal amount;
    private final String currency;

    public FareDto(LocationDto origin, LocationDto destination, BigDecimal amount, String currency) {
        this.origin = origin;
        this.destination = destination;
        this.amount = amount;
        this.currency = currency;
    }

    public LocationDto getOrigin() {
        return origin;
    }

    public LocationDto getDestination() {
        return destination;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }
}
